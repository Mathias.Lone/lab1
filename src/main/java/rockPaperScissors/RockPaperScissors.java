package rockPaperScissors;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import org.w3c.dom.DOMStringList;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
     



    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    Random rand = new Random();
    

    public void run() {
        String brukerInput; 
        int datapoeng = 0;
        int humanpoeng = 0;
        int computerInput = rand.nextInt(2);
        int brukerInt = 1;
        String wincon;
        boolean con = true;
        int count = 1;
        while (con){
            System.out.printf("Let's play round %d \n", count);
            count ++;
            brukerInput = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            brukerInt = rpsChoices.indexOf(brukerInput);
            
            while (brukerInt == -1){
                System.out.println("I do not understand " + brukerInput +". Could you try again?");
                brukerInput = readInput("Your choice (Rock/Paper/Scissors)?\n").toLowerCase();
                brukerInt = rpsChoices.indexOf(brukerInput);
            }
            
            if (brukerInt != -1){
                int diff = (computerInput - brukerInt);
                if (diff == 1){
                    wincon = "Computer wins!";
                    datapoeng ++;
                }
                else if(diff == 2){
                    wincon= "Human wins!";
                    humanpoeng ++;
                }
                else if(diff == -1){
                    wincon = "Human wins!";
                    humanpoeng ++;
                    }
                else if(diff == -2){
                    wincon = "Computer wins!";
                    datapoeng ++;
                    } 
                else {
                    wincon = "It's a tie!";
                    
                }
                System.out.printf("Human chose %s, computer chose %s. %s \n", brukerInput, rpsChoices.get(computerInput),wincon);
                System.out.printf("Score: human %d, computer %d \n", humanpoeng, datapoeng);
            }

            String yn = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (yn.equals("n")){
                System.out.println("Bye bye :)");
                con = false;
            }
            
        }
    
    }
    


    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
